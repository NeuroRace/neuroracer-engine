# General
Arduino code to control RC-Engine.
<br><br>
Derived from JetsonCar:  
http://www.jetsonhacks.com/2016/08/02/jetson-racecar-11-arduino-ros-node-for-car-control/  
https://github.com/jetsonhacks/installJetsonCar/blob/master/Arduino%20Firmware/jetsoncar/jetsoncar.ino

## Notes
The following provides general information and suggestions.

### Remove modemmanager
Ubuntu has a package called `modemmanager` preinstalled, which locks the ttyACM ports on the Jetson board. Remove the package to obtain full control.

```
sudo apt -y remove modemmanager
```

### Accessing serial
To flash the Arduino, it is necessary for some linux dustributions to add the user to specific groups. Please refer to your distribution's manual for further information.

## Editor
Atom: https://atom.io/  
Atom Plugin: http://docs.platformio.org/en/latest/ide/atom.html  
Servo: https://platformio.org/lib/show/883/Servo/installation  
Rosserial Arduino: http://platformio.org/lib/show/1634/Rosserial%20Arduino%20Library/installation  

## Patch ROS Serial for Arduino Nano
$HOME/.platformio/lib/Rosserial Arduino Library_ID1634/src/ros.h  

replace

```
...
#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega168__)
  /* downsize our buffers */
  typedef NodeHandle_<ArduinoHardware, 6, 6, 150, 150, FlashReadOutBuffer_> NodeHandle;
...
```

by


```
...
#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega168__) || defined(__AVR_ATmega32U4__)
  /* downsize our buffers */
  typedef NodeHandle_<ArduinoHardware, 6, 6, 150, 150, FlashReadOutBuffer_> NodeHandle;
...
```
